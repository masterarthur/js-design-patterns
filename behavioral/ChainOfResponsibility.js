class Calc {
    value
    constructor(initialValue = 0) {
        this.value = initialValue

        return this
    }

    add(v) {
        this.value += v

        return this
    }
    sub(v) {
        this.value -= v

        return this
    }
    dev(v) {
        this.value /= v

        return this
    }
    mult(v) {
        this.value *= v

        return this
    }

    toString() {
        return this.value
    }
}

console.log((new Calc(1)).add(3).dev(2).mult(5).dev(3))