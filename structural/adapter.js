class OldApi {
    #data

    constructor(data) {
        this.#data = data
    }

    getData() {
        return this.#data
    }
}

class NewApi {
    #data

    constructor(data) {
        this.#data = data
    }

    get data() {
        return this.#data
    }

    printData() {
        console.log(this.#data)
    }
}

class ApiAdapter {
    #api

    constructor(data) {
        this.#api = new NewApi(data)
    }

    getData() {
        return this.#api.data
    }
}

let data = {
    users: [{id: 1, name: "name1"}, {id: 2, name: "name2"}, {id: 3, name: "name3"}]
}

let oldApi = new OldApi(data)
console.log(oldApi.getData())

let newApi = new NewApi(data)
console.log(newApi.data)

let adapterApi = new ApiAdapter(data)
console.log(adapterApi.getData())