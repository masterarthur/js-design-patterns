class Image {
    constructor(url, alt = "") {
        this.url = url
        this.alt = alt
        this.data = [[]]
    }

    toHTMLString() {
        return `<img src="${this.url}" alt="${this.alt}">`
    }
}

class ImageLoaderFactory {
    #instances
    constructor() {
        this.#instances = {}
    }

    createAndLoadImage(imageUrl, altText) {
        let image = this.getImage(imageUrl)
        if(!image) {
            image = new Image(imageUrl, altText)
            this.loadImage(image)

            this.#instances[imageUrl] = image
        }

        return image
    }

    getImage(imageUrl) {
        return this.#instances[imageUrl]
    }

    loadImage(image) {
        image.data = [[Math.random(),Math.random()],[Math.random(),Math.random()]]
    }
}

const imageLoader = new ImageLoaderFactory()

let pic1 = imageLoader.createAndLoadImage("https://image.com/pic1.jpg")
let pic1WithAlt = imageLoader.createAndLoadImage("https://image.com/pic1.jpg", "picture 1")


let pic2 = imageLoader.createAndLoadImage("https://image.com/pic2.jpg")
let pic2WithAlt = imageLoader.createAndLoadImage("https://image.com/pic2.jpg", "picture 2")

console.log(JSON.stringify(pic1, null, 2))
console.log(JSON.stringify(pic1WithAlt, null, 2))
console.log(JSON.stringify(pic2, null, 2))
console.log(JSON.stringify(pic2WithAlt, null, 2))