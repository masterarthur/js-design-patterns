class Cellphone {
    constructor(name) {
        this._type = "Cellphone"
        this.name = name
    }

    pushButton(button) {
        console.log(`You've pushed ${button}`)
    }
}

class Smartphone {
    constructor(name) {
        this._type = "Smartphone"
        this.name = name
    }


    touch(x,y) {
        console.log(`You've touched at (${x}, ${y})`)
    }
}


class PhoneFactory {
    static buyPhone(price = 666, name = "noBrand") {
        return this.modify(price > 999 ? new Smartphone(name) : new Cellphone(name), price)
    }

    static modify(phone, price) {
        Object.defineProperty(phone, "price", {
            value: price,
            writable: false
        })

        phone.call = function call(phoneNumber="911") {
            console.log(`Calling ${phoneNumber} from ${this._type} ${this.name}`)
        }

        return phone
    }
}

let cellPhone = PhoneFactory.buyPhone(123, "Nokia")
cellPhone.pushButton("#")
cellPhone.call("666")

let smartPhone = PhoneFactory.buyPhone(1200)
smartPhone.touch(1,23)
smartPhone.call()