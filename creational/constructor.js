class Hero {
    constructor(name = "hero", specialAbility = "nothing") {
        this.name = name;
        this.specialAbility = specialAbility

        
    }

    getDetails() {
        return `${this.name} can ${this.specialAbility}`
    }

}

let ironMan = new Hero("Iron Man", "fly")

console.log(ironMan.getDetails())