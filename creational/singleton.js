class God {
    static #INSTANCE

    constructor(name) {
        if(God.#INSTANCE)
            return God.#INSTANCE

        this.name = name

        return God.#INSTANCE = this;
    }

    displayName() {
        console.log(`God's name is ${this.name}`)
    }
}

let satan = new God("Satan")
satan.displayName()
let jesus = new God("Jesus")
jesus.displayName()
